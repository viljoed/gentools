import sys, os, glob

# Set working directories and parameters
inWrkDir = r"C:\Users\Lavoiegen\Documents\Geomatics\_InProgress\Data\ExternalLC\Raster\Product\Rasterize"
suffix = ".tif"
suffixWild = "*.tif"
rasName = "inRaster"

# Set counter and empty list
count = 0
outFileList = []

# Create file list 
def selectFilenameList(inDir, typeExt):           
    os.chdir(inDir)
    items = os.listdir(".")

    print "List of selected " + typeExt + " filenames: " 
    fileList = []
    for names in items:        
        if names.endswith(typeExt):
            fileList.append(names)
    return fileList     

fileNameList = selectFilenameList(inWrkDir, suffix)
print fileNameList
print "------------"
   
# Change file name from list        
def changeFileName(inDir, startCount, fileList, newName, typeExt):
    for file in fileList:
        startCount += 1        
        # Split file to obtain basename
        (baseName, ext) = os.path.splitext(file)       
        # New filename and file based on existing basename
        newFilename = newName+str(startCount)+ typeExt         
        newFile = inDir + "\\" + newFilename
        outFileList.append(newFilename)
        print "Previous file: {} / new file: {}".format(file, newFilename)     
              
changeFileName(inWrkDir, count, fileNameList, rasName, suffix)
print "------------"
print "List of renamed files: " + str(outFileList)
print "------------"
