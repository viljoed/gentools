import os, glob

# Set working directories and testing parameters
inWrkDir = r"E:\Geomatics\_InProgress\Data\Raster\LC\External\Rasterize"
suffixWild = "*.tif"
suffix = ".tif"

# Select filenames with specified extension within a directory
# using filename pattern matching method and wildcard
def selectFilename(inDir, typeExt):
    print "Selected " + typeExt + " filenames: "
    os.chdir(inDir)
    for file in glob.glob(typeExt):        
        print file

selectFilename(inWrkDir, suffixWild)
print "1------------"

# List filenames with specified extensions within a directory
# String method returns true if the string ends with specified suffix
# Syntax: str.endswith(suffix[, start[, end]])
def selectFilenameList1(inDir, typeExt):
    print "List selected " + typeExt + " filenames: "
    print "Ensure to provide start and end start parameters to obtain correct result"
    filenameList = [f for f in os.listdir(inDir)
                if any(f.endswith(ext) for ext in typeExt)]
    print filenameList

selectFilenameList1(inWrkDir, suffix)
print ("2------------")

# List filenames with specified extension within a directory
# os.listdir function to search through a given path for all files
# that endswith specified extension
def selectFilenameList2(inDir, typeExt):           
    os.chdir(inDir)
    items = os.listdir(".")

    print "List of selected " + typeExt + " filenames: " 
    fileList = []
    for names in items:        
        if names.endswith(typeExt):
            fileList.append(names)
    print fileList     

selectFilenameList2(inWrkDir, suffix)
print "3------------"

# Select files using os.listdir function to search
# through a given path for all files that endswith specified extension
def selectFile(inDir, typeExt):
    print "Selected " + typeExt + " files: "
    for file in os.listdir(inDir):
        if file.endswith(typeExt):
            print(os.path.join(inDir, file))

selectFile(inWrkDir, suffix)
print "4------------"

# List filenames using filename pattern matching method
def selectFileList(inDir, typeExt):
    print "List selected " + typeExt + " files: "
    fileList = glob.glob(os.path.join(inDir, typeExt))
    print (fileList)
    
selectFileList(inWrkDir, suffixWild)
print ("5------------")
