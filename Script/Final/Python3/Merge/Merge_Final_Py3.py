import sys, os, subprocess
from osgeo import gdal

# Sets working directory and user parameters
wrkDir = r"E:\Geomatics\_InProgress\Data\Raster\LC\Internal\TIF\QC\Original"
output_name = "LC_Merge_QC_Original.tif"

# Selects and sorts list of tif files in working directory
suffix = ".tif"
files = [f for f in os.listdir(wrkDir)
         if any(f.endswith(ext) for ext in suffix)]
files.sort() # sorts files in ascending order
print ("List of land cover tif files to merge: ")
print (files)
print ("------------")

# Builds list of gdal_merge system parameters as string
listCMD = [] # empty list

for strFile in files:
    listCMD.append(strFile) # Specifies input files

listCMD.append("-n") # Specifies which pixel value will be set as noData and not merged
listCMD.append("0") # Sets 0 as NoData Value
listCMD.append("-o") # Specifies output files
listCMD.append(output_name) # Output file name
listCMD.append("-pct") # Grabs pseudo-color table from 1st input file for final output - assumes all files use same color table

print ("List of gdal_merge parameters")
print(listCMD)
print ("------------")

# Merges tif files directly in script using gdal_merge
# Last image in list copied over earlier ones
os.chdir(wrkDir) # Ensure in correct working directory
sys.path.append(r"C:\OSGeo4W64\bin") # Adds gdal environmental variable to system path
import gdal_merge as gm
gm.main(listCMD)
print ("LC tif files merged and saved here: ", wrkDir)
print ("------------")

##Command line should look like this (order parameter can vary):
##python C:\Python36\Scripts\gdal_merge.py -n 0
##L1_1_0.tif L1_1_2.tif L1_1_3.tif L2_120_0.tif L2_200_0.tif L3_121_0.tif
##L4_132_0.tif L4_150_0.tif L4_175_0.tif -o LC_Merge.tif -pct




    
        
