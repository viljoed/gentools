import sys, os, glob
import georaster as gr

# Working directory as string
wrkDir = r"E:\Geomatics\_InProgress\Data\Raster\LC\Internal\TIF\Canada"

# List of filenames with tif extensions within a directory
# Python string method returns true if the string ends with specified suffix
# Syntax: str.endswith(suffix[, start[, end]])
suffix = ".tif"
dirFiles = [f for f in os.listdir(wrkDir)
            if any(f.endswith(ext) for ext in suffix)]
print (dirFiles)
print ("------------")

# Full paths and filenames within a directory
for file in os.listdir(wrkDir):
    if file.endswith(".tif"):
        print(os.path.join(wrkDir, file))

print ("------------")
print ("------------")

# List of full paths with filenames with tif extensions within a directory
# Python filename pattern matching method
dirFiles = glob.glob(os.path.join(wrkDir, "*.tif"))
print (dirFiles)
print ("------------")

# Filenames with tif extensions within a directory
os.chdir (r"E:\Geomatics\_InProgress\Data\Raster\LC\Internal\TIF\Canada")
for file in glob.glob("*.tif"):
    print(file)

print ("------------")
print ("------------")

print ("List of sorted directory files using os.listdir method:")
dirFiles.sort()
print (dirFiles)
print ("------------")
print ("List of reverse sorted directory files using os.listdir method:")
dirFiles.sort(reverse=True)
print (dirFiles)

print ("------------")
print ("------------")

# Sorts files in directory in reverse order and appends to a list
# Python os.walk method generates filenames in a directory tree
# Syntax: os.walk(top[, topdown=True[, onerror=None[, followlinks=False]]])
print ("List of reverse sorted directory file using os.walk method:")
for path, dirs, files in os.walk(wrkDir):
    files = [f for f in os.listdir(wrkDir)
            if any(f.endswith(ext) for ext in suffix)]
    files.sort(reverse=True)
    print (files)

    print ("------------")
    print ("------------")

# Georaster python module that provides tool to work with raster quickly and flexibly
# https://pypi.python.org/pypi/georasters/
    print ("Reverse sorted paths, filenames and object attributes using georaster:")
    counter = 0
    for filename in files:
        counter = counter +1
        rasterpath = (os.path.join(path, filename))
        print ("#" + str(counter))
        print ("Full path ", rasterpath)
        print ("File ", filename)
        raster = gr.SingleBandRaster(rasterpath)
        print ("Object: " + str(raster))
        print ("Object band data type: ", str(raster.dtype))
        print ("Object array of band data: ", str(raster.r))
        cellsize = gr.SingleBandRaster.get_pixel_size(raster)
        print ("x and y pixel size: ", str(cellsize))
        extent = gr.SingleBandRaster.get_extent_latlon(raster)
        print ("Geographic extent of raster: ", str(extent))
        print (raster.r[1])
        rasterproj = gr.SingleBandRaster._load_ds(raster, rasterpath)
        myproj = raster.srs
        print ("Projection details: ", str(myproj))
        print ("------------")
    
sys.exit()          
        

        
