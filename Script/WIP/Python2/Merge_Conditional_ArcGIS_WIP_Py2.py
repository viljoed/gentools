import sys, os, glob
import arcpy
from arcpy import env
from arcpy.sa import *

# Check out extension license
if arcpy.CheckExtension("Spatial") == "Available":
    arcpy.CheckOutExtension("Spatial")
else:
    print "Spatial Analyst not available"
    sys.exit()


os.chdir(os.path.dirname(os.path.abspath(__file__)))
# Set environment settings and working directories
env.overwriteOutput = True
env.workspace = r"..\Data\Raster"
inWrkDir = r"..\Data\Raster\Rasterize"
outWrkDir = r"..\Data\Raster\Merge"

# Set user parameters
outputName = "LC_Conditional_Merge.tif"
suffixWild = "*.tif"

# Set counter and empty list
count = 0
outFileList = []

# Create file list
rasterList = glob.glob(os.path.join(inWrkDir, suffixWild))
print "List selected " + suffixWild + " files: "
print rasterList
print "------------"

# Modify raster attribute table
rasters = list()
for raster in rasterList:
    # Build table
    arcpy.BuildRasterAttributeTable_management(raster, "Overwrite")
    # Create temporary raster object
    count += 1
    rasters.append(Raster(raster))
    print "Creating rasObj" + str(count)

# Set conditional parameters
inTrueRaster = rasters[0]
inFalseConstant = 0
#whereClause = ((rasObj1==rasObj2) and (rasObj2==rasObj3) and (rasObj3==rasObj4) and (rasObj1==rasObj4))
#whereClause = rasters[0] == rasters[1]
#whereClause = rasObj1 == rasObj2
print "Executing conditional merge..."

# Execute conditional merge using Con tool
outRaster = Con(rasters[0] == rasters[1], inTrueRaster, inFalseConstant)

# Save output
outRaster.save(os.path.join(outWrkDir, outputName))

print "Conditional merge completed and saved here: " + outWrkDir

# Release extension license
arcpy.CheckInExtension("Spatial")

