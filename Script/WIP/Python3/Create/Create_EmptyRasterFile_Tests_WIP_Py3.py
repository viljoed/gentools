import unittest, sys, os
from osgeo import gdal, ogr, osr

_scriptPath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(_scriptPath)
os.chdir(_scriptPath)

import Create_EmptyRasterFile_WIP_Py3 as ce

class CreateRasters(unittest.TestCase):
    print("Testing #1")
    def test_create_lists_from_text(self):
        in_text_path = r"C:\Users\Lavoiegen\Documents\gentools\Data\Create\Other\GF1List2017_final1.txt"
        keywords = ["NOMOSAIC 0", "MOSAIC 0"]
        full_list, sub_list = ce.create_lists_from_text(in_text_path, keywords)
        self.assertEqual(394, len(full_list))
        self.assertEqual(344, len(sub_list))

    print("Testing #2")
    def test_get_path_row(self):
        file_rec = 'GF1_WFV4_W62.0_N46.7_20170608_L1A0002416196 7028 2017-06-08 MOSAIC 0'
        expect = ('7','028')
        actual = ce.get_path_row(file_rec)
        self.assertEqual(expect, actual)

        file_rec = 'GF1_WFV3_W100.5_N52.1_20170819_L1A0002550575 33024 2017-08-19 NOMOSAIC 0'
        expect = ('33', '024')
        actual = ce.get_path_row(file_rec)
        self.assertEqual(expect, actual)

    print("Testing #3")
    def test_get_date(self):
        file_rec = 'GF1_WFV1_W99.4_N49.7_20170720_L1A0002498129 32026 2017-07-20 NOMOSAIC 0'
        expect = '20170720'
        actual = ce.get_date(file_rec)
        self.assertEqual(expect, actual)

    print("Testing #4")
    def test_rename_file_from_list(self):
        in_text_path = r"C:\Users\Lavoiegen\Documents\gentools\Data\Create\Other\GF1List2017_final1.txt"
        keywords = ["NOMOSAIC 0", "MOSAIC 0"]
        full_list, sub_list = ce.create_lists_from_text(in_text_path, keywords)
        out_dir = r"E:\Geomatics\_InProgress\Data\Raster\PIX\Empty"
        out_list = ce.rename_file_from_list(sub_list, out_dir)
        self.assertEqual(344, len(out_list))
        self.assertEqual(344, len(sub_list))

    print("Testing #5")
    def test_get_vector_datasource(self):
        in_filepath = r"C:\Users\Lavoiegen\Documents\gentools\Data\Create\Vector\WRS2\STB-EOS_LS8_WRS2_Index.shp"
        ds = ce.get_vector_datasource(in_filepath)
        self.assertIsNotNone(ds)
        ds = None

    print("Testing #6")
    def test_reproject_vector(self):
        in_filepath =  r"C:\Users\Lavoiegen\Documents\gentools\Data\Create\Vector\WRS2\STB-EOS_LS8_WRS2_Index.shp"
        in_pix = "E:\Geomatics\_InProgress\Data\Raster\LC\Internal\PIX\Extract\ACGEO_2016_CI_AB_30m_v1.pix"
        target_proj = ce.get_Proj4(in_pix, "PCIDSK")
        src_proj = ce.get_projection(in_filepath, "ESRI Shapefile")
        out_path = os.path.dirname(in_filepath)
        ce.reproject_vector(in_filepath, target_proj)

    print("Testing #7")
    def test_create_empty_raster(self):
        shp_filepath = r"C:\Users\Lavoiegen\Documents\gentools\Data\Create\Vector\WRS2\Subset\STB-EOS_LS8_WRS2_Index_Reproj_ID12_13026.shp"
        vector_extents = ce.get_vector_extent(shp_filepath)
        raster_extents = ce.get_raster_extent_from_vector(vector_extents, 30)
        ce.create_empty_raster(shp_filepath, raster_extents[1], raster_extents[2], 1, "PCIDSK", gdal.GDT_Byte, vector_extents[1], vector_extents[4], 30)
        # Parameters: in_filename, XRes, YRes, nb_band, driver_name, etype, x_min, y_max, pixel_size

    print("Testing #8")
    def test_align_raster_command(self):
        raster_filepath = r"E:\Geomatics\_InProgress\Data\Raster\PIX\GF1\Empty\prelimGF1_pp50022_20170525.pix"
        in_filename = os.path.basename(raster_filepath)
        in_basename = os.path.splitext(in_filename)[0]
        out_filename = in_basename + "_Align.pix"
        out_folder = os.path.dirname(raster_filepath)
        proj4 = ce.get_Proj4(raster_filepath, "PCIDSK")

        ce.align_raster(raster_filepath, "PCIDSK", 30, 30, proj4, gdal.GDT_Byte, out_folder)
        # Parameters: in_filepath, driver_name, XRes, YRes, ds_proj, etype, out_folder

    print ("Testing completed")

if __name__ == "__main__":
    unittest.main()

