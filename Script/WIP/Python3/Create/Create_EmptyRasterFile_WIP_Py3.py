import os, glob, re, shutil, shapefile # Install pyshp-1.2.12
from osgeo import gdal, ogr, osr
from zipfile import ZipFile

# Set working directories
in_folder_ZIP = r"R:\Crop_Mapping\Data\Classification\2016_National\4_Mosaic\8_Distribution\PIX"
out_folder_ZIP = r"E:\Geomatics\_InProgress\Data\Raster\LC\Internal\PIX\Extract"
in_folder_PIX = r"E:\Geomatics\_InProgress\Data\Raster\PIX\GF1\Align"
out_folder_PIX = r"E:\Geomatics\_InProgress\Data\Raster\PIX\GF1\Empty"
in_folder_shp = r"E:\Geomatics\_InProgress\Data\Vector\WRS2\CANADA"
out_folder_shp = r"E:\Geomatics\_InProgress\Data\Vector\WRS2\CANADA\Subset"

# Set user parameters
in_text_path = r"E:\Geomatics\_InProgress\Data\Other\Text\GF1List2017_final1.txt"
in_shp_path = r"E:\Geomatics\_InProgress\Data\Vector\WRS2\CANADA\STB-EOS_LS8_WRS2_Index.shp"
out_shp_path = r"E:\Geomatics\_InProgress\Data\Vector\WRS2\CANADA\STB-EOS_LS8_WRS2_Index_Reproj.shp"
keywords = ["NOMOSAIC 0", "MOSAIC 0"]
res = 30

def main():

    # Create list of raster filenames based on specified naming convention
    lists = create_lists_from_text(in_text_path, keywords) # Returns full list and sublist matching keywords
    rename_list = rename_file_from_list(lists[1], out_folder_PIX)
    print("Text file contains #{l} files and sublist #{s} files".format(l=(len(lists[0])),s=(len(rename_list))))
    print("--------")

    # Compares attributes extracted from list against list of attributes from shapefile
    pathrow_values = create_list_from_list(rename_list) # Extract pathrow values from files
    int_pr_values_unique = set([int(i) for i in pathrow_values]) # Modifies string values to unique integer values
    field_values = get_fieldvalues(get_vector_datasource(in_shp_path), "PR") # Extract field attribute values
    shared_values = check_list(pathrow_values, field_values) # Compare all values
    print("{} unique pathrow combinations, {} shared combination values".format(len(int_pr_values_unique), len(shared_values)))
    print("--------")

    # Extract zip files
    extract_all_zip(in_folder_ZIP, out_folder_ZIP)
    print("--------")

    # Extract spatial reference from source raster
    # Identical for all files, extract from first file only
    pix_files = glob.glob(os.path.join(out_folder_ZIP, "*.pix"))
    proj4_filepath = pix_files[0]
    proj4 = get_Proj4(proj4_filepath,"PCIDSK")
    print("Source projection (Proj4 format): {}".format(proj4))
    print("--------")

    # Reproject shapefile using projection extracted from raster
    reproject_vector(in_shp_path, proj4)

    # Create individual shapefile for each WRS2 path/row combination
    subset_vector(out_shp_path, int_pr_values_unique, out_folder_shp)
    set_vector_projection(out_folder_shp, get_projection(out_shp_path, "ESRI Shapefile"), out_folder_shp)
    print ("{} subsetted and projected shapefiles".format(int(len(os.listdir(out_folder_shp))/4)))
    print("--------")

    # Create empty pix file with shapefile dimensions and raster projection
    for file in os.listdir(out_folder_shp):
        if file.endswith(".shp"):
            # Get raster columns and rows from shapefile dimensions
            vector_extents = get_vector_extent(os.path.join(out_folder_shp, file)) # Tuple contain 5 lists: PR ID, xMin, xMax, yMin, yMax
            raster_extents = get_raster_extent_from_vector(get_vector_extent(os.path.join(out_folder_shp, file)), res) # Tuple contain 3 lists: PR values and X/Y extents
            # Get path row ID for each shapefile
            pathrow_fr_shp = re.sub("\D", "", raster_extents[0])

            # Create empty pix files for each file record in text file and identified shapefile path row ID
            for pix_file in rename_list:
                parts = pix_file.split("_")
                pathrow_fr_list = re.sub("\D", "", parts[2])
                if pathrow_fr_list == pathrow_fr_shp:
                    x_res = raster_extents[1] # Nb of columns
                    y_res = raster_extents[2] # Nb of rows
                    # Parameters: filename, XRes, YRes, nb_band, driver_name, etype, x_min, y_max, pixel_size
                    create_empty_raster(pix_file, x_res, y_res, 1, "PCIDSK", gdal.GDT_Byte, vector_extents[1], vector_extents[4], res)

    pix_files = glob.glob(os.path.join(out_folder_PIX, "*.pix"))
    print("{} empty PIX files created and saved here: {}".format(len(pix_files), out_folder_PIX))
    print("--------")

    # Set raster projection and noData value
    for empty_pix_file in pix_files:
        set_empty_raster(get_raster_datasource(empty_pix_file, "PCIDSK"), proj4, 0)
        # Reproject images using gdal warp function
        align_raster(empty_pix_file, "PCIDSK", res, res, proj4, gdal.GDT_Byte, in_folder_PIX)
        # Parameters: in_filepath, XRes, YRes, ds_proj, etype
    print("{} PIX files projected and saved here: {}".format(len(pix_files), out_folder_PIX))
    print("{} PIX files aligned and saved here: {}".format(len(pix_files), in_folder_PIX))
    print("--------")

    print("Script completed!")

def create_lists_from_text(in_filepath, in_keys):
    """Create list from text file, files are selected based on the presence of keywords in file record"""
    full_list = [] # List of files in text file
    sub_list = []  # Sublist of selected files
    with open(in_filepath, 'r+') as f:
        # Create sublist using keywords
        for line in f:
            full_list.append(line.rstrip())
            if any(x in line for x in in_keys):
                sub_list.append(line)
        return full_list, sub_list

def get_path_row(file_rec):
    """Given GF1_WFV1_W99.4_N49.7_20170720_L1A0002498129 32026 2017-07-20 NOMOSAIC 0
     return  '32', '026'"""
    parts = file_rec.split()
    return parts[1][:-3], parts[1][-3:]

def get_date(file_rec):
    """Given GF1_WFV1_W99.4_N49.7_20170720_L1A0002498129 32026 2017-07-20 NOMOSAIC 0
     return  '20170720'"""
    parts = file_rec.split("_")
    return parts[4]

def format_path_row(path, row):
    """If path > 10, then format pathrow as ppPathRow, else as pPathRow"""
    if int(path) > 10:
        p = 'pp{}'.format(path)
    else:
        p = 'p{}'.format(path)
    return "{}{}".format(p,row)

def rename_file_from_list(in_list, out_dir):
    """Given a list rename files and appends to new list with full directory path"""
    out_list = []  # List of empty pix files in directory
    for name in in_list:
        path, row = get_path_row(name)
        pathrow = format_path_row(path, row)
        file_date = get_date(name)
        new_name = "prelimGF1_{}_{}.pix".format(pathrow, file_date)
        out_list.append(os.path.join(out_dir, new_name))
    return out_list

def create_list_from_list(in_list):
    """Given a list of filenames extract path row combination values and create new list"""
    out_list = []  # List of pathrow combinations extracted from list
    for line in in_list:
        parts = line.split("_")
        pathrow = re.sub("\D", "", parts[2])
        out_list.append(pathrow)
    return out_list

def get_vector_datasource(in_filepath):
    """Given full path to vector file, returns an OGR datasource for the ESRI driver format"""
    driver = ogr.GetDriverByName("ESRI Shapefile")
    # Open and read file in read only mode
    ds = driver.Open(in_filepath, 0)
    if ds == None:
        raise Exception("Unable to open {}".format(in_filepath))
    return ds

def is_field_in_shp(ds, field_name):
    """Given a vector data source, check for the presence of a specified field name in shapefile"""
    layer = ds.GetLayer()
    layer_def = layer.GetLayerDefn()
    for i in range(layer_def.GetFieldCount()):
        if layer_def.GetFieldDefn(i).name == field_name:
            return True
    ds = None
    return False

def get_fieldvalues(ds, field_name):
    """Given a vector data source, extract attribute field values"""
    is_field_in_shp(ds, field_name)
    layer = ds.GetLayer()
    layer_def = layer.GetLayerDefn()
    feature = layer.GetNextFeature()
    field_values = set([feature.GetFieldAsInteger(field_name) for feature in layer])
    return field_values

def check_list(a, b):
    """Given 2 lists compare shared items"""
    shared_ab = [item for item in a if item not in b]
    return shared_ab

def extract_all_zip(in_dir, out_dir):
    """List all zip files within an input directory, opens in read mode, extract and
    outputs to output directory"""
    unzip_files = []  # List of unzipped pix files in directory
    zip_files = [f for f in os.listdir(in_dir)
                 if any(f.endswith(ext) for ext in ".zip")]
    for zip_file in zip_files:
        unzip_files.append(zip_file)
        # Open zip files
        with ZipFile(os.path.join(in_dir, zip_file), 'r') as zip:
            # Display zip file contents
            zip.printdir()
            # Extract zip files to local folder
            zip.extractall(out_dir)
    return unzip_files

def create_list_from_unzip():
    """Create a list from extracted zip files in directory"""
    unzip_list = extract_all_zip(in_folder_ZIP, out_folder_ZIP)

def get_raster_datasource(in_filepath, driver_name):
    """Given full path to raster file, returns an OGR datasource"""
    driver = gdal.GetDriverByName(driver_name)
    readonly = 0
    # Open and read file
    ds = gdal.Open(in_filepath, readonly)
    if ds == None:
        raise Exception("Unable to open {}".format(in_filepath))
    else:
        return ds

def get_projection(in_filepath, driver_name):
    """Given vector or raster datasource, return projection details"""
    if driver_name == "ESRI Shapefile":
        ds = get_vector_datasource(in_filepath)
        layer = ds.GetLayer()
        proj = layer.GetSpatialRef()
        return proj
    else:
        ds = get_raster_datasource(in_filepath, driver_name)
        proj = ds.GetProjection()
        return proj

def get_Proj4(in_filepath, driver_name):
    """Given raster datasource, return spatial reference object"""
    spatialRef = osr.SpatialReference()
    spatialRef.ImportFromWkt(get_projection(in_filepath, driver_name))
    spatialRefProj = spatialRef.ExportToProj4()
    return spatialRefProj

def reproject_vector(src_filepath, target_proj):
    """Reproject input datasource projection to another projection using Proj4 format"""

    # Extract target folder and filename from source file
    src_filename = os.path.basename(src_filepath)
    src_basename = os.path.splitext(src_filename)[0]
    target_basename = src_basename + "_Reproj"
    target_filename = src_basename + "_Reproj.shp"
    target_folder = os.path.dirname(src_filepath)
    target_filepath = os.path.join(target_folder, target_filename)

    # Import target spatial reference from raster
    target_sr = osr.SpatialReference()
    target_sr.ImportFromProj4(target_proj)

    # Create vector datasource object
    driver = ogr.GetDriverByName("ESRI Shapefile")
    ds = driver.Open(src_filepath, 0)
    layer = ds.GetLayer()

    # Import source spatial reference from vector
    layer_sr = layer.GetSpatialRef()

    # Create target layer, delete if already exist
    if os.path.exists(target_filepath):
        driver.DeleteDataSource(target_filepath)
    target = driver.CreateDataSource(target_filepath)

    # Use Well-known binary (WKB) format to specify geometry
    target_layer = target.CreateLayer(target_basename, geom_type=ogr.wkbPolygon)
    feature_def = layer.GetLayerDefn()
    trans = osr.CoordinateTransformation(layer_sr, target_sr)
    src_feature = layer.GetNextFeature()
    while src_feature:
        geom = src_feature.GetGeometryRef()
        geom.Transform(trans)
        feature = ogr.Feature(feature_def)
        feature.SetGeometry(geom)
        target_layer.CreateFeature(feature)
        feature.Destroy()
        src_feature.Destroy()
        src_feature = layer.GetNextFeature()
    ds.Destroy()
    target.Destroy()

    # Convert geometry to ESRI Well-known Text (WKT) format for export to projection file
    target_proj_filepath = os.path.join(target_folder, target_basename + ".prj")
    target_sr.MorphToESRI()
    proj = open(target_proj_filepath, "w")
    proj.write(target_sr.ExportToWkt())
    proj.close()

    # Copy database information from destination file
    src_Dbf = os.path.join(target_folder, src_basename + ".dbf")
    target_Dbf = os.path.join(target_folder, target_basename + ".dbf")
    shutil.copyfile(src_Dbf, target_Dbf)

def subset_vector(in_filepath, in_list, out_path):
    """" Subset a large dataset by filering through matching field attributes and save into individual shapefiles"""

    r = shapefile.Reader(in_filepath) # Create reader instance of shapefile

    # Extract basename from input file
    in_filename = os.path.basename(in_filepath)
    in_basename = os.path.splitext(in_filename)[0]

    # Create field names to search through
    fields = r.fields[1:]
    field_names = [field[0] for field in fields]

    # Loop through each polygon and dbf record and create new shapefiles
    for i, shpRec in enumerate(r.shapeRecords()): # Provide ID for each shapefile
        attributes = dict(zip(field_names, shpRec.record))
        # Select records within list
        if attributes['PR'] in in_list:

            # Create individual shapefiles from polygons/records
            shape = shpRec.shape # Polygon
            rec = shpRec.record # DBF record
            w = shapefile.Writer(shapefile.POLYGON) # Create writer instance for polygon geometry
            w.fields = list(r.fields) # Copy fields to writer instance
            w._shapes.append(shape) # Add geometry to new shapefile list of shapes
            w.records.append(rec) # Add DBF records ''
            out_filename = "{}_ID{}_{}.shp".format(in_basename, i, rec[7]) # Create unique name with ID and PR#
            out_filepath = os.path.join(out_path, out_filename)

            # Save shapefiles to disk
            w.save(out_filepath)

def set_vector_projection(in_dir, in_proj, out_path):
    """ Creates projection file for shapefile within directory"""
    source_prj = in_proj
    shp_files = glob.glob(os.path.join(in_dir, "*.shp"))
    for shp in shp_files:
        # Extract filename and filepath
        shp_name = os.path.basename(shp)
        shp_basename = os.path.splitext(shp_name)[0]
        prj_name = shp_basename + "_Reproj.shp"
        prj_filepath = os.path.join(out_path, shp_basename + ".prj")

        # Create projection file
        source_prj.MorphToESRI()
        proj = open(prj_filepath, "w")
        proj.write(source_prj.ExportToWkt())
        proj.close()

def get_vector_extent(in_filepath):
    """Given vector data source, return dimensions"""

    # Extract basename and ID
    shp_name = os.path.basename(in_filepath)
    shp_basename = os.path.splitext(shp_name)[0]
    parts = shp_basename.split('_')
    pathrow_id = parts[-1]

    # Extract dimensions
    ds = get_vector_datasource(in_filepath)
    layer = ds.GetLayer()
    xMin, xMax, yMin, yMax = layer.GetExtent()
    return pathrow_id, xMin, xMax, yMin, yMax

def get_raster_extent_from_vector(in_dim, pixel_size):
    """Convert layer extent to image pixel coordinates"""
    pathrow_ID = in_dim[0]

    xMin = in_dim[1]
    xMax = in_dim[2]
    yMin = in_dim[3]
    yMax = in_dim[4]

    cols = int((xMax-xMin)/pixel_size)
    rows = int((yMax-yMin)/pixel_size)
    return pathrow_ID, cols, rows

def get_raster_extent(ds):
    """Given a raster datasource gets the raster extent"""
    geoTransform = ds.GetGeoTransform()
    xMin = geoTransform[0]
    yMax = geoTransform[3]
    xMax = xMin + geoTransform[1] * ds.RasterXSize
    yMin = yMax + geoTransform[5] * ds.RasterYSize
    ds = None
    return xMin, yMin, xMax, yMax

def create_empty_raster(in_filename, XRes, YRes, nb_band, driver_name, etype, x_min, y_max, pixel_size):
    """Given filename creates empty raster files with specific resolution, bit depth, number of bands and dimensions"""
    empty_raster = gdal.GetDriverByName(driver_name).Create(in_filename, XRes, YRes, nb_band, etype)
    # Create(constchar * pszName, intnXSize, intnYSize, intnBands, GDALDataTypeeType, char ** papszOptions)
    empty_raster.SetGeoTransform((x_min, pixel_size, 0, y_max, 0, -pixel_size))
    if not empty_raster:
        print("Error: could not create dataset")
        ds = None
        return False

def set_raster_projection(ds, in_proj):
    """Given raster datasource write projection using an input raster projection"""
    spatialRef = osr.SpatialReference()
    spatialRef.ImportFromProj4(in_proj)
    ds.SetProjection(spatialRef.ExportToProj4())

def set_raster_noData(ds, noData):
    """Given raster datasource wset noData"""
    band = ds.GetRasterBand(1)
    band.SetNoDataValue(noData)

def set_empty_raster(ds, in_proj, noData):
    set_raster_projection(ds, in_proj)
    set_raster_noData(ds, noData)

def align_raster(in_filepath, driver_name, XRes, YRes, ds_proj, etype, out_folder):
    """Given an orthorectified image, reproject/align using gdal warp function"""

    # Create filename and folder for output
    in_filename = os.path.basename(in_filepath)
    in_basename = os.path.splitext(in_filename)[0]
    out_filename = in_basename + "_Align.pix"
    out_filepath = os.path.join(out_folder, out_filename)

    # Warp options: http://gdal.org/python/osgeo.gdal-module.html#WarpOptions
    warp_opts = gdal.WarpOptions(format=driver_name, xRes=XRes, yRes=YRes, targetAlignedPixels = True, dstSRS=ds_proj,
                outputType=etype, workingType=etype, srcNodata=0)

    # Warp(destNameOrDestDS, srcDSOrSrcDSTab, **kwargs)
    gdal.Warp(out_filepath, in_filepath, options=warp_opts)

    # gdal command line parameters
    # command = "gdalwarp -tap -et 0.0 -r cubic -srcnodata 0 -rpc -overwrite -to \"RPC_DEM_INTERPOLATION=CUBIC\" " \
    #           "-to \"RPC_DEM_MISSING_VALUE=0\" -to \"RPC_DEM=" + dem_path + "\" -tr " + str(image_obj.raw.res) + " " \
    #           + str(image_obj.raw.res) + " -t_srs \"" + image_obj.raw.proj + "\" " + os.path.normpath(image) + " " + \
    #           outimage_path
    # p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

if __name__ == "__main__":
    main()

