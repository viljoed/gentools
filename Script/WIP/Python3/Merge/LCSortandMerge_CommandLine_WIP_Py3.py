import sys, os, subprocess
from osgeo import gdal

# Sets working directory and parameters
wrkDir = r"C:\Users\Lavoiegen\Documents\Geomatics\_InProgress\Data\LC"
fileName = "LC_Merge.tif"
filePath = wrkDir + "\\" + fileName
scriptPath = r"C:\Python36\Scripts\gdal_merge.py"

# Selects and sorts list of tif files in working directory
suffix = ".tif"
files = [f for f in os.listdir(wrkDir)
         if any(f.endswith(ext) for ext in suffix)]
files.sort()
absPathFiles = list()
for f in files:
    absPathFiles.append(os.path.join(wrkDir,f).replace('\\','/'))
print("Sorted land cover tif files to merge: {}".format(files))
print ("------------")

# Joins tif files
fileList = ' '.join(absPathFiles)
print ("Sorted land cover tif files with path to merge: {}".format(fileList))
print ("------------")

# Build gdal system parameters for command line
cmd = "python " + scriptPath + " -n 0 -o " + filePath + " -pct " + fileList
print ("Command line: {}".format(cmd))
print ("------------")

# Call subprocess module to run command line
print ("LC tif files merged and saved here: ", wrkDir)
subprocess.call(cmd,shell=True)
print ("------------")

## Command line should look like this:
## python C:\Python36\Scripts\gdal_merge.py -n 0
## L1_1_0.tif L1_1_2.tif L1_1_3.tif L2_120_0.tif L2_200_0.tif L3_121_0.tif
## L4_132_0.tif L4_150_0.tif L4_175_0.tif -o LC_Merge.tif -pct
## if include '-pct' last image in list copied over earlier ones
## Order of arguments flexible
## Include merged file path to save to specific directory
## http://gdal.org/gdal_merge.html

        
