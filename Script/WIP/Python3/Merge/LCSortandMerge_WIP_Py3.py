import sys, os, subprocess
from osgeo import gdal

# Sets working directory and system path for gdal_merge
wrkDir = r"C:\Users\Lavoiegen\Documents\Geomatics\_InProgress\Data\LC"
colorTable = r"C:\Users\Lavoiegen\Documents\Geomatics\_InProgress\aci_crop_classifications_iac_classifications_des_cultures.csv"
os.chdir(wrkDir)

# Selects and sorts list of tif files in working directory
suffix = ".tif"
files = [f for f in os.listdir(wrkDir)
         if any(f.endswith(ext) for ext in suffix)]
files.sort()
print ("List land cover tif files to merge: ")
print (files)
print ("------------")

# Builds list of gdal_merge system arguments as string
listCMD = [] # empty list
listCMD.append("-n") # Sets NoData Value
listCMD.append("0") # 0 NoData Value

for strFile in files:
    listCMD.append(strFile) # Specifies input files

listCMD.append("-o") # Specifies output files
listCMD.append("LC_Merge.tif") # Output file name
listCMD.append("-pct") # Grabs pseudo-color table
#listCMD.append(colorTable)

print ("List of gdal_merge arguments")
print(listCMD)
print ("------------")

##Command line should look like this:
##python C:\Python36\Scripts\gdal_merge.py -n 0
##L1_1_0.tif L1_1_2.tif L1_1_3.tif L2_120_0.tif L2_200_0.tif L3_121_0.tif
##L4_132_0.tif L4_150_0.tif L4_175_0.tif -o LC_Merge.tif
##-pct C:\Users\Lavoiegen\Documents\Geomatics\_InProgress\
##aci_crop_classifications_iac_classifications_des_cultures.csv

# Merges tif files directly in script using gdal_merge
# Last image in list copied over earlier ones
sys.path.append(r"C:\OSGeo4W64\bin")
import gdal_merge as gm
gm.main(listCMD)
print ("LC tif files merged and saved here: ", wrkDir)
print ("------------")

sys.exit()

# Merges listed tif files in command line using Python string.format 
# and gdal_merge methods
# Last image in list copied over earlier ones
# 3 arguments including color table input and ignores nodata pixels from files from being merged
# http://gdal.org/gdal_merge.html

##cmd = r"python C:\Python36\Scripts\gdal_merge.py -n 0 {} -o {} -pct {}"
##cmd = cmd.format(fileList, "LC_Merge.tif", colorTable)
##print ("Command line:")
##print (cmd)
### Runs command line in command prompt
##os.system(cmd)
##print ("LC tif files merged and saved here: ", wrkDir)
##print ("------------")

# Subprocess module replaces os.system
##subprocess.call(cmd,shell=True)
##print ("------------")





    
        
