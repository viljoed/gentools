import sys, os, glob
from osgeo import gdal, ogr

# Sets working directories and user parameters
wrkDirIn = r"E:\Geomatics\_InProgress\Data\Vector\LC\4Processing"
wrkDirOut = r"E:\Geomatics\_InProgress\Data\Raster\LC\External\Rasterize\New"
fieldName = "Code" # Contains class values
pSize = 30 # Resolution
noDataValue = 0

# Rasterize shapefile into 8bit raster layer
count = 0

shpFileList = glob.glob(os.path.join(wrkDirIn, "*.shp"))
shpList = os.listdir(wrkDirIn)

for shpFile in shpList:
    if shpFile.endswith(".shp"):
        shpFilePath = os.path.join(wrkDirIn, shpFile)
        (shpName, ext) = os.path.splitext(shpFile)        
        print ("Vector file # {nb:d} {p}".format(nb=(count+1), p=shpFilePath))

        # Open selected data source with ogr
        ds = ogr.Open(shpFilePath)
        if not ds:
             print("Error: could not open data source")  # none if can't open
             break
        else:
             print("Extract attributes from data source:")

        # Get data source driver, attributes and properties
        driver = ds.GetDriver()
        print("OGR Driver: {n}".format(n=driver.name))

        # Extract layer attributes
        layer = ds.GetLayer()
        defn = layer.GetLayerDefn()
        fieldCount = defn.GetFieldCount()
        fieldDef = defn.GetFieldDefn(1)
        name = fieldDef.GetName()
        feature = layer.GetNextFeature()
        fieldValues = set([feature.GetFieldAsInteger(fieldName) for feature in layer])
        fields = [defn.GetFieldDefn(n).name for n in range(defn.GetFieldCount())]
        print (fields)
        if  fieldName not in fields:
            print("Error: field attribute does not exist")
            break
        else:
            print("Code value(s): {f}".format(f=fieldValues))

        # Extract layer properties
        spRef = layer.GetSpatialRef()
        proj4 = spRef.ExportToProj4()
        xMin, xMax, yMin, yMax = layer.GetExtent()
        print("Projection: {proj4}".format(proj4=proj4))
        print("Extents:", xMin, xMax, yMin, yMax)
        print("------------")

        # Define raster parameters
        rasterName = shpName + ".tif"
        rasterFile = wrkDirOut + "\\" + rasterName
        cols = int((xMax-xMin)/pSize)
        rows = int((yMax-yMin)/pSize)
        print("Raster file # {nb:d} {p}".format(nb=(count + 1), p=rasterFile))

        # Create 8bit raster target
        rasterOutput = gdal.GetDriverByName("GTiff").Create(rasterFile, cols, rows, len(fieldValues), gdal.GDT_Byte)
        if not rasterOutput:
            print("Error: could not create dataset")
            break
        else:
            print("Create raster target and set properties")

        # Set raster properties
        rasterOutput.SetProjection(spRef.ExportToWkt())
        rasterOutput.SetGeoTransform((xMin, pSize, 0, yMax, 0, -pSize))
        rasterBand = rasterOutput.GetRasterBand(1)
        bandType = gdal.GetDataTypeName(rasterBand.DataType)
        rasterBand.SetNoDataValue(noDataValue)
        fill = rasterBand.Fill(0)
        print ("NoData value: {}".format(rasterBand.GetNoDataValue()))
        print ("Data type: {}".format(bandType))
        print("Projection: {proj}".format(proj=rasterOutput.GetProjection()))
        print("Extents: {ext}".format(ext=rasterOutput.GetGeoTransform()))
        print("# columns / rows: C{c} R{r}".format(c=cols, r=rows))

        # Rasterize
        # gdal.RasterizeLayer(target_ds, #band, source_layer, burn_values, options = ["BURN_VALUE_FROM=Code"])
        # "ALL_TOUCHED=TRUE", # Rasterize all pixels touched by polygons
        err = gdal.RasterizeLayer(rasterOutput,[1], layer, burn_values=[0], options = ["ALL_TOUCHED=TRUE", "ATTRIBUTE=" + name])
        if err != 0:
            raise Exception("Error rasterizing layer: {err}".format(err=err))
        else:
            print ("Rasterization completed: {}".format(wrkDirOut))
            print ("------------")
        count = count+1

        # Close datasets
        ds = None
        rasterOutput = None