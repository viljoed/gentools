import os
import unittest
import wip_rasterizer as wr

_script_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(_script_path)

class WIP_RasterizerTests(unittest.TestCase):
    def test__get_datasource(self):
        ds = wr._get_datasource(r"..\TestData\Province.shp")
        self.assertIsNotNone(ds)
        ds = None

    def test__get_shp_basename(self):
        shp = r"..\TestData\Can_Mjr_Cities.shp"
        expected = "Can_Mjr_Cities"
        self.assertEqual(expected, wr._get_shp_basename(shp))

    def test__get_unique_values(self):
        wr.field_name = "DWELL1996"
        ds = wr._get_datasource(r"..\TestData\Province.shp")
        expected = 13
        values = wr._get_unique_values(ds)
        actual = len(values)
        self.assertEqual(expected, actual)
        ds = None

    def test__is_field_in_shp(self):
        ds = wr._get_datasource(r"..\TestData\Province.shp")

        wr.field_name = "BadField"
        expected = False
        actual = wr._is_field_in_shp(ds)
        self.assertEqual(expected, actual)

        wr.field_name = "DWELL1996"
        expected = True
        actual = wr._is_field_in_shp(ds)
        self.assertEqual(expected, actual)
        ds = None

    def test_rasterize(self):
        outtif = r"..\TestData\raster\province.tif"
        if os.path.exists(outtif):
            os.remove(outtif)
        wr.field_name = "DWELL1996"
        wr.in_folder_shps = r"..\TestData"
        wr.out_folder_tifs = r"..\TestData\raster"
        wr.pixel_size = .01
        wr.rasterize()

if __name__ == "__main__":
    unittest.main()

