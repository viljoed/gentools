import sys, os
from osgeo import gdal, ogr
import glob

# Sets working directories and user parameters
in_folder_shps = r"C:\Users\Lavoiegen\Documents\Geomatics\_InProgress\Data\ExternalLC\Vector\4Processing"
out_folder_tifs = r"C:\Users\Lavoiegen\Documents\Geomatics\_InProgress\Data\ExternalLC\Raster\Product\4Processing"
field_name = "Code" # Contains class values
pixel_size = 30 # Resolution
noDataValue = 0

def main():
    global in_folder_shps
    global out_folder_tifs
    global field_name
    global pixel_size

    if len(sys.argv) != 5:
        print ("Usage:  wip_rasterizer.py in_folder_shps out_folder_tifs field_name pixel_size")
        sys.exit()

    in_folder_shps = sys.argv[1]
    if not os.path.exists(in_folder_shps):
        print ("{} does not exist".format(in_folder_shps))
        return

    out_folder_tifs = sys.argv[2]
    if not os.path.exists(out_folder_tifs):
        print ("{} does not exist".format(out_folder_tifs))
        return

    field_name = sys.argv[3]
    pixel_size = sys.argv[4]
    try:
        pixel_size = int(pixel_size)
    except:
        print ("Pixel size must be an integer")

    rasterize()

def rasterize():
    """Rasterize all SHP files in in_folder_shps and write all output TIF files
       to out_folder_tifs.  The output TIF files will have the same base name
       as the input SHP file"""

    # Rasterize shapefile into 8bit raster layer
    shpFiles = glob.glob(os.path.join(in_folder_shps,"*.shp"))
    for shpFile in shpFiles:
        print ("Rasterizing",shpFile,"...")
        success = _rasterize_shp(shpFile)
        if success == False:
            print ("Failed to rasterize {}".format(shpFile))
        else:
            print ("{} rasterized.".format(shpFile))

def _rasterize_shp(shp):
    """Given the full path to a shp files, rasterize to a TIF file in
       out_folder_tifs the same base name as the SHP.  The output raster will
       have one band that will contain all distinct integer values in field_name.
    """

    ds = _get_datasource(shp)

    if not _is_field_in_shp(ds):
        print ("{} does not exist in {}".format(field_name, ds.GetName()))
        ds = None
        return False

    # TODO: Check if field_name is a number without decimals (e.g. integer, long, etc.)

    field_values = _get_unique_values(ds)
    field_values = [str(val) for val in field_values]
    outfile = open(ds.GetName().replace('.shp','_{}.txt'.format(field_name)), 'w')
    for fv in field_values:
        outfile.writelines("{}\n".format(fv))
    outfile.close()

    layer = ds.GetLayer()
    spRef = layer.GetSpatialRef()
    # proj4 = spRef.ExportToProj4()
    xMin, xMax, yMin, yMax = layer.GetExtent()

    # Define raster parameters
    #
    shp_no_ext = os.path.basename(shp).split('.')[0]
    rasterName = "{}.tif".format(shp_no_ext)
    rasterFile = os.path.join(out_folder_tifs, rasterName)
    cols = int((xMax-xMin)/pixel_size)
    rows = int((yMax-yMin)/pixel_size)

    # Create 8bit raster target (same number of bands as distinct values in field_name
    #
    #rasterOutput = gdal.GetDriverByName("GTiff").Create(rasterFile, cols, rows, len(field_values), gdal.GDT_Byte)

    # Create 8-bit raster target with one band
    #
    rasterOutput = gdal.GetDriverByName("GTiff").Create(rasterFile, cols, rows, 1, gdal.GDT_Byte)

    if not rasterOutput:
        print("Error: could not create dataset")
        ds = None
        return False

    # Set raster properties
    rasterOutput.SetProjection(spRef.ExportToWkt())
    rasterOutput.SetGeoTransform((xMin, pixel_size, 0, yMax, 0, -pixel_size))
    rasterBand = rasterOutput.GetRasterBand(1)
    bandType = gdal.GetDataTypeName(rasterBand.DataType)
    rasterBand.SetNoDataValue(noDataValue)
    fill = rasterBand.Fill(0)

    # Rasterize
    # Usage:  RasterizeLayer(*args, **kwargs)
    # RasterizeLayer(Dataset dataset, int bands, Layer layer, void * pfnTransformer=None, void * pTransformArg=None,
    #                int burn_values=0, char ** options=None, GDALProgressFunc callback=0, void * callback_data=None)
    #                -> int
    #
    # gdal.RasterizeLayer(target_ds, #band, source_layer, burn_values, options = ["BURN_VALUE_FROM=Code"])
    # "ALL_TOUCHED=TRUE", # Rasterize all pixels touched by polygons

    err = gdal.RasterizeLayer(rasterOutput,[1], layer, burn_values=[0], options = ["ALL_TOUCHED=TRUE", "ATTRIBUTE=" + field_name])
    if err != 0:
        print ("Error rasterizing layer: {err}".format(err=err))
        ds = None
        rasterOutput = None
        return False

    # Close datasets
    ds = None
    rasterOutput = None
    return True

def _get_datasource(shp):
    """Given full path to SHP, return an OGR datasource"""
    driver = ogr.GetDriverByName("ESRI Shapefile")
    readonly = 0
    ds = driver.Open(shp, readonly)
    if ds == None:
        raise Exception("Unable to open {}".format(shp))
    return ds

def _shp_has_field(field_def):
    """Given an OGR layer definition object, return True if field_name is one
       of the field
    """
    pass

def _get_unique_values(ds):
    """"Given an OGR datasource, return the distinct values for field_name
        Ref: https://gis.stackexchange.com/questions/115053/how-to-find-all-
                   distinct-values-from-a-single-column-of-an-attribute-table-with"""

    sql = 'SELECT DISTINCT {} FROM {}'.format(field_name,
                                                 _get_shp_basename(ds.GetName()))
    features = ds.ExecuteSQL(sql)
    values = list()
    for feature in features:
        values.append(feature.GetField(0))
    return values

def _get_shp_basename(shp):
    """Given a full path to a shapefile, return the name of the SHP
       with no path or extension"""

    return os.path.basename(shp).split('.')[0]

def _is_field_in_shp(ds):
    """Given an OGR DataSource, return True if field_name is one of the fields"""

    layer = ds.GetLayer()
    layer_def = layer.GetLayerDefn()
    for i in range(layer_def.GetFieldCount()):
        if layer_def.GetFieldDefn(i).name == field_name:
            return True
    return False

if __name__ == "__main__":
    main()